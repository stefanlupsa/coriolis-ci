package coriolis.ci

import coriolis.ci.Utils
import groovy.json.JsonOutput
import net.sf.json.JSONArray
import org.apache.commons.lang3.SerializationUtils

def deep_copy_hashmap(LinkedHashMap old_map) {
    return SerializationUtils.clone(old_map);
}

def download_appliance_certificate(String appliance_ip) {
    stage("Download appliance certificate") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def CERT_URL = "http://${appliance_ip}:9001/coriolis-ca.crt"
                dir("${WORKSPACE}/tmp") {
                    sh("curl -o ./coriolis-ca.crt ${CERT_URL}")
                    stash name: "shared-coriolis-ca"
                }
                sh("rm -rf ./tmp/coriolis-ca.crt")
            }
        }
    }
}

def get_appliance_providers() {
    withCredentials([string(credentialsId: global_vars.VSPHERE_CONN_INFO_CREDS_ID,
                            variable: "VSPHERE_CONN_INFO"),
                     string(credentialsId: global_vars.VSPHERE_ENV_INFO_CREDS_ID,
                            variable: "VSPHERE_ENV_INFO")]) {
        return [
            "vmware": [
                "connection_info": VSPHERE_CONN_INFO,
                "env_info": VSPHERE_ENV_INFO,
            ],
        ]
    }
}

def deploy_base_appliance(String platform,
                          String connection_info,
                          String environment_info,
                          String vm_id) {

    stage("Deploy base appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def command = ("coriolis-cd boot base appliance " +
                               "--platform ${platform} " +
                               "--connection-info '${connection_info}' " +
                               "--environment-info '${environment_info}' " +
                               "--vm-identifier ${vm_id} " +
                               "-f value")
                def utils = new Utils()
                def stdout = utils.run_coriolis_tools_script(command, true)
                return stdout.tokenize().last()
            }
        }
    }
}

def build_appliance(String appliance_ip,
                    String export_providers_list,
                    String import_providers_list,
                    String custom_repo_owner_names,
                    String custom_repo_branches,
                    String coriolis_docker_repo_url = global_vars.CORIOLIS_DOCKER_REPO_URL,
                    String coriolis_docker_repo_branch = global_vars.CORIOLIS_DOCKER_REPO_BRANCH) {

    stage("Build appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                withCredentials([usernamePassword(credentialsId: global_vars.DOCKER_REGISTRY_CREDS_ID,
                                                  usernameVariable: "DOCKER_USER_NAME",
                                                  passwordVariable: "DOCKER_USER_PASS"),
                                 usernamePassword(credentialsId: global_vars.APPLIANCE_CREDS_ID,
                                                  usernameVariable: "APPLIANCE_USER_NAME",
                                                  passwordVariable: "APPLIANCE_USER_PASS"),
                                 file(credentialsId: global_vars.REPO_SSH_KEY_CREDS_ID,
                                      variable: "REPO_SSH_KEY")]) {
                    def command = ("coriolis-cd build coriolis components " +
                                   "--host ${appliance_ip} " +
                                   "--username ${APPLIANCE_USER_NAME} " +
                                   "--password ${APPLIANCE_USER_PASS} " +
                                   "--key-path ${REPO_SSH_KEY} " +
                                   "--remote-key-path /root/bitbucket_key " +
                                   "--docker-registry ${global_vars.DOCKER_REGISTRY} " +
                                   "--docker-registry-user ${DOCKER_USER_NAME} " +
                                   "--docker-registry-password '${DOCKER_USER_PASS}' " +
                                   "--export-providers ${export_providers_list} " +
                                   "--import-providers ${import_providers_list} " +
                                   "--provider-custom-repo-names '${custom_repo_owner_names}' " +
                                   "--provider-custom-branch-names '${custom_repo_branches}' " +
                                   "--coriolis-docker-repo-url ${coriolis_docker_repo_url} " +
                                   "--coriolis-docker-branch-name ${coriolis_docker_repo_branch}")
                    def utils = new Utils()
                    utils.run_coriolis_tools_script(command)
                }
            }
        }
    }
}

def deploy_appliance(String appliance_ip,
                     String coriolis_docker_repo_url = global_vars.CORIOLIS_DOCKER_REPO_URL,
                     String coriolis_docker_repo_branch = global_vars.CORIOLIS_DOCKER_REPO_BRANCH) {
    stage("Deploy appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                withCredentials([usernamePassword(credentialsId: global_vars.DOCKER_REGISTRY_CREDS_ID,
                                                  usernameVariable: "DOCKER_USER_NAME",
                                                  passwordVariable: "DOCKER_USER_PASS"),
                                 usernamePassword(credentialsId: global_vars.APPLIANCE_CREDS_ID,
                                                  usernameVariable: "APPLIANCE_USER_NAME",
                                                  passwordVariable: "APPLIANCE_USER_PASS")]) {
                    def command = ("coriolis-cd deploy coriolis components " +
                                   "--host ${appliance_ip} " +
                                   "--username ${APPLIANCE_USER_NAME} " +
                                   "--password ${APPLIANCE_USER_PASS} " +
                                   "--docker-registry ${global_vars.DOCKER_REGISTRY} " +
                                   "--docker-registry-user ${DOCKER_USER_NAME} " +
                                   "--docker-registry-password '${DOCKER_USER_PASS}' " +
                                   "--coriolis-docker-repo-url ${coriolis_docker_repo_url} " +
                                   "--coriolis-docker-branch-name ${coriolis_docker_repo_branch}")
                    def utils = new Utils()
                    utils.run_coriolis_tools_script(command)
                }
            }
        }
    }
}

def get_appliance_default_ssh_info(String appliance_ip) {
    withCredentials([usernamePassword(credentialsId: global_vars.APPLIANCE_CREDS_ID,
                                      usernameVariable: "APPLIANCE_USER_NAME",
                                      passwordVariable: "APPLIANCE_USER_PASS")]) {
        def remote = [:]
        remote.name = "coriolis-appliance"
        remote.host = appliance_ip
        remote.user = APPLIANCE_USER_NAME
        remote.password = APPLIANCE_USER_PASS
        remote.allowAnyHosts = true
        return remote
    }
}

def cleanup_appliance(String platform,
                      String connection_info,
                      String environment_info,
                      String vm_id) {

    stage("Cleanup appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def command = ("coriolis-cd delete instance " +
                               "--platform '${platform}' " +
                               "--connection-info '${connection_info}' " +
                               "--environment-info '${environment_info}' " +
                               "--vm-identifier '${vm_id}'")
                def utils = new Utils()
                utils.run_coriolis_tools_script(command)
            }
        }
    }
}

def new_appliance_password(LinkedHashMap appliance_ssh_info) {
    stage("Set new appliance Password") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                // Generate password: length 20, must use caps, lowercase & digits, non-pronounceable algorithm
                def new_password = sh(script: "cloudbase-strong-password", returnStdout: true).trim()
                def new_admin_password = sh(script: "cloudbase-strong-password", returnStdout: true).trim()
                // Change password in appliance
                sshCommand remote: appliance_ssh_info, command: "source /etc/kolla/admin-openrc.sh && openstack user set --password '${new_admin_password}' admin", failOnError: true
                sshCommand remote: appliance_ssh_info, command: "sed -i 's/^export OS_PASSWORD=.*/export OS_PASSWORD=\"${new_admin_password}\"/g' /etc/kolla/admin-openrc.sh"
                sshCommand remote: appliance_ssh_info, command: "echo -e \"${new_password}\\n${new_password}\" | passwd root", failOnError: true
                // Check connectivity and clear any saved session
                appliance_ssh_info['password'] = new_password
                sshCommand remote: appliance_ssh_info, command: "rm -f ~/.bash_history && history -c && rm -rf ~/.docker", failOnError: true
                echo 'Appliance Password: ' + new_password
                echo 'Admin Password: ' + new_admin_password
                return appliance_ssh_info
            }
        }
    }
}

def enable_console_script(LinkedHashMap appliance_ssh_info) {
    stage("Enabling console script") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sshCommand remote: appliance_ssh_info, command: "cp /root/coriolis-docker/rc.local /etc/rc.local", failOnError: true
                sshCommand remote: appliance_ssh_info, command: "chmod +x /etc/rc.local && systemctl enable rc-local", failOnError: true
            }
        }
    }
}

def cleanup_appliance_logs(LinkedHashMap appliance_ssh_info) {
    stage("Cleaning up appliance coriolis logs") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sshCommand remote: appliance_ssh_info, command: "echo /var/log/coriolis/*.log | xargs -n1 cp /dev/null", failOnError: true
            }
        }
    }
}
def cleanup_appliance_landscape_sysinfo_cache(LinkedHashMap appliance_ssh_info) {
    stage("Cleaning up appliance landscape sysinfo cache") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sshCommand remote: appliance_ssh_info, command: "cp /dev/null /var/lib/landscape/landscape-sysinfo.cache", failOnError: false
            }
        }
    }
}

def reboot_appliance(LinkedHashMap appliance_ssh_info) {
    stage("Reboot the appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sshCommand remote: appliance_ssh_info, command: "export HISTSIZE=0 && reboot", failOnError: false
            }
        }
    }
}

def export_appliance(String platform,
                     String connection_info,
                     String environment_info,
                     String vm_id,
                     String appliance_password = "") {

    stage("Export appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def command = ("coriolis-cd export instance " +
                               "--platform '${platform}' " +
                               "--connection-info '${connection_info}' " +
                               "--environment-info '${environment_info}' " +
                               "--vm-identifier '${vm_id}' " +
                               "--target-path '${WORKSPACE}'")
                def utils = new Utils()
                utils.run_coriolis_tools_script(command)
                sh("mv ${WORKSPACE}/${vm_id}.ova ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.ova")
                sh("sha256sum ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.ova | awk '{print \$1}' > ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.ova.sha256sum")
                sh("chmod 644 ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.ova ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.ova.sha256sum")
                if (appliance_password) {
                    sh("echo '${appliance_password}' > ${global_vars.APPLIANCE_EXPORT_DIR}/${vm_id}.passwd")
                }
            }
        }
    }
}

def remove_exported_appliance(String appliance_id) {
    stage("Remove exported appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sh("rm -f ${global_vars.APPLIANCE_EXPORT_DIR}/${appliance_id}.ova")
                sh("rm -f ${global_vars.APPLIANCE_EXPORT_DIR}/${appliance_id}.passwd")
                sh("rm -f ${global_vars.APPLIANCE_EXPORT_DIR}/${appliance_id}.ova.sha256sum")
            }
        }
    }
}

def import_appliance(String platform,
                     String connection_info,
                     String environment_info,
                     String vm_id,
                     String appliance_url) {

    stage("Import appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def command = ("coriolis-cd deploy appliance " +
                               "--platform '${platform}' " +
                               "--connection-info '${connection_info}' " +
                               "--environment-info '${environment_info}' " +
                               "--vm-identifier '${vm_id}' " +
                               "--appliance-url '${appliance_url}' " +
                               "-f value")
                def utils = new Utils()
                def stdout = utils.run_coriolis_tools_script(command, true)
                return stdout.tokenize().last()
            }
        }
    }
}

def get_config(String config_file_creds_id) {
    stage("Get config") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                withCredentials([file(credentialsId: config_file_creds_id,
                                      variable: "CONFIG_FILE_PATH")]) {
                    def config = readYaml (file: CONFIG_FILE_PATH)
                    return config
                }
            }
        }
    }
}

def validate_config(String config_file_creds_id, JSONArray tests_ids) {
    stage("Validate config") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                withCredentials([file(credentialsId: config_file_creds_id,
                                      variable: "CONFIG_FILE_PATH")]) {
                    def utils = new Utils()
                    utils.run_coriolis_tools_script("coriolis-cd validate test config --config-file-path '${CONFIG_FILE_PATH}'")
                    if (tests_ids.size() == 0) {
                        throw new Exception("The tests IDs list is empty")
                    }
                    def config = readYaml (file: CONFIG_FILE_PATH)
                    for (test_id in tests_ids) {
                        def test = config['tests'].find { p -> p.id == test_id }
                        if (!test) {
                            throw new Exception("The test ID '${test_id}' is not present in the '${config_file_creds_id}' CI testing file")
                        }
                    }
                }
            }
        }
    }
}

def expose_coriolis(LinkedHashMap appliance_ssh_info) {
    stage("Expose Coriolis") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                int retry_count = 1, max_retries = 10, retry_timeout = 10;
                while (retry_count <= max_retries) {
                    try {
                        def command = ("coriolis-cd expose coriolis " +
                                       "--host '${appliance_ssh_info.host}' " +
                                       "--username '${appliance_ssh_info.user}' " +
                                       "--password '${appliance_ssh_info.password}'")
                        def utils = new Utils()
                        utils.run_coriolis_tools_script(command)
                        break
                    } catch (Exception e) {
                        if (retry_count == max_retries) {
                            throw e
                        }
                        println("Expose failed. Retry count ${retry_count}/${max_retries}. Retrying in ${retry_timeout} seconds")
                        sleep(retry_timeout)
                        retry_count += 1
                        continue
                    }
                }
            }
        }
    }
}

def add_coriolis_licence(LinkedHashMap appliance_ssh_info, LinkedHashMap coriolis_api_creds_info) {
    stage("Add Coriolis licence") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sshCommand remote: appliance_ssh_info, command: "timedatectl set-timezone UTC", failOnError: true
                checkout scm
                dir("${WORKSPACE}/tmp") {
                    unstash "shared-coriolis-ca"
                }
                withCredentials([string(credentialsId: global_vars.LICENSING_GENERATOR_BIN_CREDS_ID,
                                        variable: "GENERATOR_BIN_URL")]) {
                    withEnv(["APPLIANCE_IP=${appliance_ssh_info.host}",
                             "OS_PROJECT_DOMAIN_NAME=Default",
                             "OS_USER_DOMAIN_NAME=Default",
                             "OS_PROJECT_NAME=admin",
                             "OS_TENANT_NAME=admin",
                             "OS_USERNAME=${coriolis_api_creds_info.user}",
                             "OS_PASSWORD=${coriolis_api_creds_info.password}",
                             "OS_CACERT=${WORKSPACE}/tmp/coriolis-ca.crt",
                             "OS_AUTH_URL=https://${coriolis_api_creds_info.host}:${global_vars.APPLIANCE_KEYSTONE_PORT}/v3"]) {
                        def utils = new Utils()
                        utils.run_coriolis_tools_script("${WORKSPACE}/appliance/scripts/add_coriolis_licence.sh")
                    }
                }
                sh("rm -rf ./tmp/coriolis-ca.crt")
            }
        }
    }
}

def get_coriolis_api_creds_info(LinkedHashMap appliance_ssh_info) {
    stage("Get Coriolis API creds info") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                def python_script = ("import yaml;" +
                                     "f = open('/etc/kolla/passwords.yml', 'r');" +
                                     "p = yaml.safe_load(f);" +
                                     "f.close();" +
                                     "print(p['keystone_admin_password'])")
                def api_creds_info = [:]
                api_creds_info.host = appliance_ssh_info.host
                api_creds_info.user = "admin"
                api_creds_info.password = sshCommand(remote: appliance_ssh_info,
                                                     failOnError: true,
                                                     command: "python3 -c \"${python_script}\"")
                return api_creds_info
            }
        }
    }
}

def add_coriolis_endpoints(LinkedHashMap config, LinkedHashMap coriolis_api_creds_info) {
    stage("Add providers endpoints") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                for (endpoint in config['endpoints']) {
                    if (endpoint.type != "metal") {
                        checkout scm
                        dir("${WORKSPACE}/tmp") {
                            unstash "shared-coriolis-ca"
                        }
                        def command = ("coriolis-cd add coriolis endpoint " +
                                       "--name '${endpoint.name}' " +
                                       "--description '${endpoint.name}' " +
                                       "--type '${endpoint.type}' " +
                                       "--connection-info '${JsonOutput.toJson(endpoint.connection_info)}' " +
                                       "--ca-cert '${WORKSPACE}/tmp/coriolis-ca.crt' " +
                                       "--auth-url 'https://${coriolis_api_creds_info.host}:${global_vars.APPLIANCE_KEYSTONE_PORT}/v3' " +
                                       "--user-name '${coriolis_api_creds_info.user}' " +
                                       "--user-password '${coriolis_api_creds_info.password}'")
                        def utils = new Utils()
                        utils.run_coriolis_tools_script(command)
                        sh("rm -rf ./tmp/coriolis-ca.crt")
                    }
                }
            }
        }
    }
}


def create_source_testing_env(String platform,
                              Integer test_index,
                              LinkedHashMap test,
                              LinkedHashMap appliance_ssh_info,
                              LinkedHashMap coriolis_api_creds_info,
                              LinkedHashMap endpoint,
                              LinkedHashMap export_provider,
                              String provider_connection_info
                              ) {
    def testing_config = [:]
    testing_config.test = test
    testing_config.export_provider = export_provider
    if (endpoint.type in global_vars.SUPPORTED_PUBLIC_CLOUDS) {
        def resource_base_name = "${JOB_NAME}-${BUILD_ID}-${test_index}"
        def utils = new Utils()
        if (endpoint.type == "azure") {
            def command = "source ./appliance/scripts/utils/lib-azure.sh && " +
                          "az-login " +
                          "\"${endpoint.connection_info.service_principal_credentials.client_id}\" " +
                          "\"${endpoint.connection_info.service_principal_credentials.client_secret}\" " +
                          "\"${endpoint.connection_info.tenant}\" &&" +
                          "az-create-rg ${resource_base_name} ${export_provider.env_info.location} && " +
                          "az-deploy-simple-vm ${resource_base_name} ${resource_base_name} " +
                          "${test.vm_config.os_version} " +
                          "${test.vm_config.username} ${test.vm_config.password}"
            utils.run_coriolis_tools_script(command)
            testing_config.test.vm_name = resource_base_name.toString()
            testing_config.export_provider.network_name = "${resource_base_name}VNET/${resource_base_name}Subnet" // Azure defaults
            testing_config.export_provider.env_info.resource_group = resource_base_name.toString()
        }

        if (endpoint.type == "aws") {
            def auth_command = "source ./appliance/scripts/utils/lib-aws.sh && aws-configure " +
                               "${endpoint.connection_info.access_key_id} " +
                               "${endpoint.connection_info.secret_access_key} " +
                               "${endpoint.connection_info.region} && "
            def create_resources = auth_command + "aws-vpc-create-public ${resource_base_name}"
            def awsReply = utils.run_coriolis_tools_script(create_resources, true).readLines()
            def subnet_id = awsReply.find { obj -> obj.contains('SubnetId') }.split(":")[1].trim()
            def secgroup_id = awsReply.find { obj -> obj.contains('SecurityGroupId') }.split(":")[1].trim()
            def create_vm = auth_command + "aws-deploy-simple ${test.vm_config.os_type} ${test.vm_config.os_version} " +
                            "${resource_base_name} ${test.vm_config.ssh_key_id} ${subnet_id} ${secgroup_id}"
            utils.run_coriolis_tools_script(create_vm)
            testing_config.test.vm_name = resource_base_name.toString()
            testing_config.export_provider.network_name = awsReply.find { obj -> obj.contains('vpcId') }.split(":")[1].trim()
        }

        if (endpoint.type == "metal") {
            def deploy_bm = ("coriolis-cd baremetal machine deploy --platform ${platform} " +
                            "--connection-info '${provider_connection_info}' " +
                            "--vm-config '${JsonOutput.toJson(test.vm_config)}' " +
                            "--appliance-config '${JsonOutput.toJson(appliance_ssh_info)}' -f value")
            utils.run_coriolis_tools_script(deploy_bm)
            def register_bm = ("coriolis-cd baremetal machine register --platform ${platform} " +
                              "--connection-info '${provider_connection_info}' " +
                              "--vm-config '${JsonOutput.toJson(test.vm_config)}' " +
                              "--appliance-config '${JsonOutput.toJson(appliance_ssh_info)}' -f value")
            testing_config.test.vm_name = utils.run_coriolis_tools_script(register_bm, true).tokenize().last()
            def get_bm_network = ("coriolis-cd baremetal machine get network --id ${testing_config.test.vm_name} " +
                                 "--ca-cert '${WORKSPACE}/tmp/coriolis-ca.crt' " +
                                 "--auth-url 'https://${coriolis_api_creds_info.host}:${global_vars.APPLIANCE_KEYSTONE_PORT}/v3' " +
                                 "--user-name '${coriolis_api_creds_info.user}' " +
                                 "--user-password '${coriolis_api_creds_info.password}' -f value")
            testing_config.export_provider.network_name = utils.run_coriolis_tools_script(get_bm_network, true).split()[-2..-1].join(" ")
        }
    }
    return testing_config
}


def cleanup_source_testing_env(String platform,
                               LinkedHashMap testing_config,
                               LinkedHashMap source_endpoint,
                               String provider_connection_info) {
    if(source_endpoint.type in global_vars.SUPPORTED_PUBLIC_CLOUDS) {
        def command = ""
        def utils = new Utils()
        if (source_endpoint.type == "azure") {
            command = ("source ./appliance/scripts/utils/lib-azure.sh && " +
                       "az-login " +
                       "\"${source_endpoint.connection_info.service_principal_credentials.client_id}\" " +
                       "\"${source_endpoint.connection_info.service_principal_credentials.client_secret}\" " +
                       "\"${source_endpoint.connection_info.tenant}\" &&" +
                       "az-cleanup-rg ${testing_config.export_provider.env_info.resource_group}")
        }
        if (source_endpoint.type == "aws") {
            command = ("source ./appliance/scripts/utils/lib-aws.sh && aws-configure " +
                       "${source_endpoint.connection_info.access_key_id} " +
                       "${source_endpoint.connection_info.secret_access_key} " +
                       "${source_endpoint.connection_info.region} && " +
                       "aws-vpc-cleanup ${testing_config.export_provider.network_name}")
        }
        if (source_endpoint.type == "metal") {
            command = "coriolis-cd baremetal machine delete --platform ${platform} " +
                      "--connection-info '${provider_connection_info}' " +
                      "--vm-config '${JsonOutput.toJson(testing_config.test.vm_config)}'"
        }
        utils.run_coriolis_tools_script(command)
    }
}


def run_test(String platform,
             Integer test_index,
             LinkedHashMap test,
             LinkedHashMap source_endpoint,
             LinkedHashMap export_provider,
             LinkedHashMap import_provider,
             LinkedHashMap coriolis_api_creds_info,
             LinkedHashMap appliance_ssh_info,
             Boolean resources_cleanup = false,
             String provider_connection_info) {

    stage("${test_index} ${test.scenario}: ${export_provider.id} to ${import_provider.id}") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                checkout scm
                dir("${WORKSPACE}/tmp") {
                    unstash "shared-coriolis-ca"
                }
                def testing_config = create_source_testing_env(platform, test_index, test, appliance_ssh_info, coriolis_api_creds_info, source_endpoint, export_provider, provider_connection_info)
                if (testing_config.test.containsKey("network_map")) {
                    network_mapping = testing_config.test.network_map
                } else {
                    network_mapping = [(testing_config.export_provider.network_name): (import_provider.network_name)]
                }
                def command = ("coriolis-cd run coriolis test " +
                               "--test-scenario '${testing_config.test.scenario}' " +
                               "--origin-endpoint '${testing_config.export_provider.endpoint_name}' " +
                               "--destination-endpoint '${import_provider.endpoint_name}' " +
                               "--source-environment '${JsonOutput.toJson(testing_config.export_provider.env_info)}' " +
                               "--destination-environment '${JsonOutput.toJson(import_provider.env_info)}' " +
                               "--network-map '${JsonOutput.toJson(network_mapping)}' " +
                               "--default-storage-backend '${import_provider.get('storage_backend', '')}' " +
                               "--instance '${testing_config.test.vm_name}' " +
                               "--auth-url 'https://${coriolis_api_creds_info.host}:${global_vars.APPLIANCE_KEYSTONE_PORT}/v3' " +
                               "--ca-cert '${WORKSPACE}/tmp/coriolis-ca.crt' " +
                               "--user-name '${coriolis_api_creds_info.user}' " +
                               "--user-password '${coriolis_api_creds_info.password}' ")
                if (resources_cleanup) {
                    command += "--cleanup "
                }
                def validation_ports = testing_config.test.get('validation_ports', [])
                for (port in validation_ports) {
                    command += "--validation-port ${port} "
                }
                def utils = new Utils()
                try {
                    utils.run_coriolis_tools_script(command)
                } finally {
                    cleanup_source_testing_env(platform, testing_config, source_endpoint, provider_connection_info)
                    sh("rm -rf ./tmp/coriolis-ca.crt")
                }
            }
        }
    }
}

def update_latest_appliance(String appliance_id) {
    stage("Update latest appliance") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                sh("ln -fs ${appliance_id}.ova           ${global_vars.APPLIANCE_EXPORT_DIR}/coriolis-appliance-latest.ova")
                sh("ln -fs ${appliance_id}.ova.sha256sum ${global_vars.APPLIANCE_EXPORT_DIR}/coriolis-appliance-latest.ova.sha256sum")
                if (fileExists ("${global_vars.APPLIANCE_EXPORT_DIR}/${appliance_id}.passwd")) {
                    sh("ln -fs ${appliance_id}.passwd ${global_vars.APPLIANCE_EXPORT_DIR}/coriolis-appliance-latest.passwd")
                }
            }
        }
    }
}

def push_docker_images(LinkedHashMap appliance_ssh_info) {
    stage("Push Docker images") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                try {
                    withCredentials([usernamePassword(credentialsId: global_vars.DOCKER_REGISTRY_CREDS_ID,
                                                      usernameVariable: "DOCKER_USER_NAME",
                                                      passwordVariable: "DOCKER_USER_PASS")]) {
                        sshCommand(command: "docker login ${global_vars.DOCKER_REGISTRY} -u ${DOCKER_USER_NAME} -p '${DOCKER_USER_PASS}'",
                                   remote: appliance_ssh_info, failOnError: true)
                    }
                    sshCommand(command: "/root/coriolis-docker/coriolis-ansible push-docker-images",
                               remote: appliance_ssh_info, failOnError: true)
                } finally {
                    sshCommand(command: "docker logout ${global_vars.DOCKER_REGISTRY}",
                               remote: appliance_ssh_info, failOnError: true)
                }
            }
        }
    }
}

def collect_logs(LinkedHashMap appliance_ssh_info, LinkedHashMap coriolis_api_creds_info) {
    stage("Collect logs") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                checkout scm
                dir("${WORKSPACE}/tmp") {
                    unstash "shared-coriolis-ca"
                }
                def LOCAL_LOGS_DIR = "${WORKSPACE}/logs"
                sh("rm -rf ${LOCAL_LOGS_DIR}")
                sh("mkdir -p ${LOCAL_LOGS_DIR}")
                withEnv(["LOGS_DIR=${LOCAL_LOGS_DIR}/coriolis-logger-files",
                         "OS_PROJECT_DOMAIN_NAME=Default",
                         "OS_USER_DOMAIN_NAME=Default",
                         "OS_PROJECT_NAME=admin",
                         "OS_TENANT_NAME=admin",
                         "OS_USERNAME=${coriolis_api_creds_info.user}",
                         "OS_PASSWORD=${coriolis_api_creds_info.password}",
                         "OS_CACERT=${WORKSPACE}/tmp/coriolis-ca.crt",
                         "OS_AUTH_URL=https://${coriolis_api_creds_info.host}:${global_vars.APPLIANCE_KEYSTONE_PORT}/v3"]) {
                    def utils = new Utils()
                    utils.run_coriolis_tools_script("${WORKSPACE}/appliance/scripts/download_coriolis_logs.sh")
                }
                sh("mkdir -p ${LOCAL_LOGS_DIR}/coriolis")
                sshGet(from: "/var/log/coriolis", into: "${LOCAL_LOGS_DIR}",
                       remote: appliance_ssh_info, failOnError: true, override: true)
                sh("mkdir -p ${LOCAL_LOGS_DIR}/kolla")
                sshGet(from: "/var/log/kolla", into: "${LOCAL_LOGS_DIR}",
                       remote: appliance_ssh_info, failOnError: true, override: true)
                sh("mkdir -p ${global_vars.LOGS_DIR}/${JOB_NAME}")
                sh("rm -rf ${global_vars.LOGS_DIR}/${JOB_NAME}/${BUILD_ID}")
                sh("rm -rf ./tmp/coriolis-ca.crt")
                sh("mv ${LOCAL_LOGS_DIR} ${global_vars.LOGS_DIR}/${JOB_NAME}/${BUILD_ID}")
                println("Logs available at: ${global_vars.LOGS_BASE_URL}/${JOB_NAME}/${BUILD_ID}")
            }
        }
    }
}

def email_notification_with_status(String RECIPIENTS,String BUILD_STATUS) {
    stage("Email Notification") {
        node(global_vars.JENKINS_SLAVES_LABEL) {
            timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
                println("Sending email notification step. ")
            }
        }
        emailext (
            subject: "[ CORIOLIS CI ] Job ${env.JOB_NAME}/${BUILD_DISPLAY_NAME} build number ${env.BUILD_NUMBER} status is : $BUILD_STATUS",
            body: "Job Details:\nURL: ${env.BUILD_URL} \nConsole Output URL: ${env.BUILD_URL}console ",
            to: RECIPIENTS
        )
    }
}

return this
