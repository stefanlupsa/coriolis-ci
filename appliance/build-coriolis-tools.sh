#!/usr/bin/env bash
set -o errexit
set -o pipefail

DIR=$(dirname $0)

rm -rf "$DIR/build"
mkdir -p "$DIR/build"

DEFAULT_REPO_BRANCH="master"

git clone ${CORIOLIS_CD_REPO_URL:-"https://bitbucket.org/cloudbase/coriolis-cd.git"} \
    --branch ${CORIOLIS_CD_REPO_BRANCH:-"master"} \
    "$DIR/build/coriolis-cd"
git clone ${PYTHON_CORIOLIS_CLIENT_REPO_URL:-"https://github.com/cloudbase/python-coriolisclient.git"} \
    --branch ${PYTHON_CORIOLIS_CLIENT_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/python-coriolisclient"
git clone ${CORIOLIS_REPO_URL:-"https://github.com/cloudbase/coriolis.git"} \
    --branch ${CORIOLIS_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis"
git clone ${CORIOLIS_PROVIDER_AZURE_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-azure.git"} \
    --branch ${CORIOLIS_PROVIDER_AZURE_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-azure"
git clone ${CORIOLIS_PROVIDER_ORACLE_VM_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-oracle-vm.git"} \
    --branch ${CORIOLIS_PROVIDER_ORACLE_VM_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-oracle-vm"
git clone ${CORIOLIS_PROVIDER_OCI_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-oci.git"} \
    --branch ${CORIOLIS_PROVIDER_OCI_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-oci"
git clone ${CORIOLIS_PROVIDER_OVIRT_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-ovirt.git"} \
    --branch ${CORIOLIS_PROVIDER_OVIRT_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-ovirt"
git clone ${CORIOLIS_PROVIDER_LXD_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-lxd.git"} \
    --branch ${CORIOLIS_PROVIDER_LXD_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-lxd"
git clone ${CORIOLIS_PROVIDER_PROXMOX_REPO_URL:-"git@bitbucket.org:cloudbase/coriolis-provider-proxmox.git"} \
    --branch ${CORIOLIS_PROVIDER_PROXMOX_REPO_BRANCH:-"$DEFAULT_REPO_BRANCH"} \
    "$DIR/build/coriolis-provider-proxmox"

docker build -f "$DIR/coriolis-tools.Dockerfile" -t coriolis-tools "$DIR"
