FROM ubuntu:22.04

ARG UNAME=ubuntu
ARG GNAME=ubuntu
ARG UID=1000
ARG GID=1000


RUN groupadd --gid ${GID} ${GNAME} && \
    useradd --create-home --uid ${UID} --gid ${GID} --shell /bin/bash ${UNAME}

RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y apt-utils
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    git python3 python3-pip curl jq libz-dev libmysqlclient-dev \
    ca-certificates curl apt-transport-https lsb-release gnupg \
    build-essential libssl-dev pkg-config libcurl4-openssl-dev \
    libxml2-dev gcc python3-dev

ADD build/coriolis-cd                 /usr/local/src/coriolis-cd
ADD build/python-coriolisclient       /usr/local/src/python-coriolisclient
ADD build/coriolis                    /usr/local/src/coriolis
ADD build/coriolis-provider-azure     /usr/local/src/coriolis-provider-azure
ADD build/coriolis-provider-oracle-vm /usr/local/src/coriolis-provider-oracle-vm
ADD build/coriolis-provider-oci       /usr/local/src/coriolis-provider-oci
ADD build/coriolis-provider-ovirt     /usr/local/src/coriolis-provider-ovirt
ADD build/coriolis-provider-lxd       /usr/local/src/coriolis-provider-lxd
ADD build/coriolis-provider-proxmox       /usr/local/src/coriolis-provider-proxmox

RUN pip3 install python-novaclient python-cinderclient python-neutronclient awscli
RUN pip3 install -e /usr/local/src/python-coriolisclient
RUN pip3 install -e /usr/local/src/coriolis
RUN pip3 install -e /usr/local/src/coriolis-provider-azure
RUN pip3 install -e /usr/local/src/coriolis-provider-oracle-vm
RUN pip3 install -e /usr/local/src/coriolis-provider-oci
RUN pip3 install -e /usr/local/src/coriolis-provider-ovirt
RUN pip3 install -e /usr/local/src/coriolis-provider-lxd
RUN pip3 install -e /usr/local/src/coriolis-provider-proxmox
RUN pip3 install -e /usr/local/src/coriolis-cd

RUN curl -sL https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --dearmor | \
    tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null

RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main" | \
    tee /etc/apt/sources.list.d/azure-cli.list

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y azure-cli
