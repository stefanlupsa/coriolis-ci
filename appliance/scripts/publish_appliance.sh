#!/usr/bin/env bash
set -o errexit
set -o pipefail

if [[ $# -ne 1 ]]; then
    echo "USAGE: $0 <APPLIANCE_FILE>"
    exit 1
fi

APPLIANCES_BASE_URL="https://coriolis.blob.core.windows.net/appliances"

export AZURE_STORAGE_AUTH_MODE="key"
export AZURE_STORAGE_ACCOUNT="coriolis"

BLOB_NAME="coriolis-appliance-$(date '+%Y%m%d-%H%M').ova"
az storage blob upload --container-name "appliances" --file "$1" --name $BLOB_NAME

echo "Appliance available at: ${APPLIANCES_BASE_URL}/${BLOB_NAME}"
