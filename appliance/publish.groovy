@Library('common-lib') _


currentBuild.displayName = "#${currentBuild.number}-${params.APPLIANCE_FILE}"

stage("Publish appliance") {
    node(global_vars.JENKINS_SLAVES_LABEL) {
        timeout(global_vars.JENKINS_GLOBAL_TIMEOUT) {
            checkout scm
            sh("$WORKSPACE/appliance/scripts/publish_appliance.sh ${global_vars.APPLIANCE_EXPORT_DIR}/${params.APPLIANCE_FILE}")
        }
    }
}
