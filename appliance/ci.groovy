@Library('common-lib') _

CI = new coriolis.ci.Appliance()

BASE_APPLIANCE_PROVIDERS = CI.get_appliance_providers()

APPLIANCE_ID = "coriolis-appliance-jenkins-ci-${BUILD_ID}"
APPLIANCE_PROVIDER_CONN_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].connection_info
APPLIANCE_PROVIDER_ENV_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info

if (env.repository_full_name) {
    currentBuild.displayName = "#${currentBuild.number}-${repository_full_name}"
}

// NOTE(ibalutoiu): The Jenkins job param 'APPLIANCE_ID' value should be empty
// by default. Otherwise, the 'currentBuild.displayName' set via webhook job
// trigger will always be overwritten.
if (params.APPLIANCE_ID) {
    APPLIANCE_ID = params.APPLIANCE_ID
    currentBuild.displayName = "#${currentBuild.number}-${params.APPLIANCE_ID}"
}

def tests_ids = readJSON (text: params.TESTS_IDS)
CI.validate_config(params.CONFIG_FILE_CREDS_ID, tests_ids)

def testing_config = CI.get_config(params.CONFIG_FILE_CREDS_ID)

def appliance_ssh_info = null
def coriolis_api_creds_info = null

//NOTE(gluka): Due to bug Declarative Pipeline plugin detailed here:
//https://issues.jenkins.io/browse/JENKINS-56402
//we'll use a variable for build status
def build_status = 'SUCCESS'

try {
    def appliance_ip = CI.deploy_base_appliance(
        params.PLATFORM,
        APPLIANCE_PROVIDER_CONN_INFO,
        APPLIANCE_PROVIDER_ENV_INFO,
        APPLIANCE_ID)

    CI.build_appliance(
        appliance_ip,
        params.EXPORT_PROVIDERS,
        params.IMPORT_PROVIDERS,
        params.CUSTOM_REPO_OWNER_NAMES,
        params.CUSTOM_REPO_BRANCHES,
        params.CORIOLIS_DOCKER_REPO_URL,
        params.CORIOLIS_DOCKER_REPO_BRANCH)

    CI.deploy_appliance(
        appliance_ip,
        params.CORIOLIS_DOCKER_REPO_URL,
        params.CORIOLIS_DOCKER_REPO_BRANCH)

    appliance_ssh_info = CI.get_appliance_default_ssh_info(appliance_ip)
    CI.enable_console_script(appliance_ssh_info)
    appliance_ssh_info = CI.new_appliance_password(appliance_ssh_info)

    CI.export_appliance(
        params.PLATFORM,
        APPLIANCE_PROVIDER_CONN_INFO,
        APPLIANCE_PROVIDER_ENV_INFO,
        APPLIANCE_ID,
        appliance_ssh_info['password'])

    CI.cleanup_appliance(
        params.PLATFORM,
        APPLIANCE_PROVIDER_CONN_INFO,
        APPLIANCE_PROVIDER_ENV_INFO,
        APPLIANCE_ID)

    appliance_ssh_info.host = CI.import_appliance(
        params.PLATFORM,
        APPLIANCE_PROVIDER_CONN_INFO,
        APPLIANCE_PROVIDER_ENV_INFO,
        APPLIANCE_ID,
        "${global_vars.APPLIANCE_BASE_URL}/${APPLIANCE_ID}.ova")

    coriolis_api_creds_info = CI.get_coriolis_api_creds_info(appliance_ssh_info)
    CI.expose_coriolis(appliance_ssh_info)
    CI.download_appliance_certificate(appliance_ssh_info.host)
    CI.add_coriolis_licence(appliance_ssh_info, coriolis_api_creds_info)
    CI.add_coriolis_endpoints(testing_config, coriolis_api_creds_info)

    def testing_stages = [:]
    def test_count = 1
    for (test in testing_config['tests']) {
        if (!(test.id in tests_ids)) {
            continue
        }
        def test_index = test_count
        def current_test = CI.deep_copy_hashmap(test)
        def export_provider = CI.deep_copy_hashmap(testing_config['export_providers'].find { p -> p.id == test.export_provider_id })
        def import_provider = CI.deep_copy_hashmap(testing_config['import_providers'].find { p -> p.id == test.import_provider_id })
        def export_endpoint = CI.deep_copy_hashmap(testing_config['endpoints'].find { p -> p.name == export_provider.endpoint_name })
        testing_stages["#${test_count} ${test.id} ${test.scenario}: ${export_provider.id} to ${import_provider.id}"] = {
            CI.run_test(params.PLATFORM, test_index, current_test, export_endpoint, export_provider, import_provider,
            coriolis_api_creds_info, appliance_ssh_info, params.TESTS_RESOURCES_CLEANUP, APPLIANCE_PROVIDER_CONN_INFO)
        }
        test_count = test_count + 1
    }
    parallel testing_stages

    if (params.UPDATE_LATEST_APPLIANCE == true) {
        CI.update_latest_appliance(APPLIANCE_ID)
    }

    if (params.PUSH_DOCKER_IMAGES == true) {
        CI.push_docker_images(appliance_ssh_info)
    }
} catch (Exception e) {
    CI.remove_exported_appliance(APPLIANCE_ID)
    build_status = 'FAILED'
    throw e

} finally {
    if ((appliance_ssh_info != null) && (coriolis_api_creds_info != null)) {
        CI.collect_logs(appliance_ssh_info, coriolis_api_creds_info)
    }
    if (params.CLEANUP_APPLIANCE == true) {
        CI.cleanup_appliance(
            params.PLATFORM,
            APPLIANCE_PROVIDER_CONN_INFO,
            APPLIANCE_PROVIDER_ENV_INFO,
            APPLIANCE_ID)
    } else {
        println("Job parameter CLEANUP_APPLIANCE is false.")
        println("The appliance deletion stage will be skipped.")
    }
    if (params.EMAIL_NOTIFICATION == true) {
        CI.email_notification_with_status(params.RECIPIENTS_LIST, build_status)
    } else {
        println("Job parameter EMAIL_NOTIFICATION is false.")
        println("No email notification will be sent.")
    }
}
