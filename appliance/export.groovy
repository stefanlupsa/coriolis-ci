@Library('common-lib') _

currentBuild.displayName = "#${currentBuild.number}-${params.VM_IDENTIFIER}"

CI = new coriolis.ci.Appliance()

APPLIANCES_PROVIDERS = CI.get_appliance_providers()

APPLIANCE_CONN_INFO = APPLIANCES_PROVIDERS[params.PLATFORM].connection_info
APPLIANCE_ENV_INFO = APPLIANCES_PROVIDERS[params.PLATFORM].env_info


CI.export_appliance(
    params.PLATFORM,
    APPLIANCE_CONN_INFO,
    APPLIANCE_ENV_INFO,
    params.VM_IDENTIFIER)

if (params.CLEANUP_APPLIANCE == true) {
    CI.cleanup_appliance(
        params.PLATFORM,
        APPLIANCE_CONN_INFO,
        APPLIANCE_ENV_INFO,
        params.VM_IDENTIFIER)
} else {
    println("Job parameter CLEANUP_APPLIANCE is false.")
    println("The appliance deletion stage will be skipped.")
}
