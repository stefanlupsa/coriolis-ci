@Library('common-lib') _

import groovy.json.JsonOutput

currentBuild.displayName = "#${currentBuild.number}-${params.VM_IDENTIFIER}"

CI = new coriolis.ci.Appliance()

BASE_APPLIANCE_PROVIDERS = CI.get_appliance_providers()
if (params.BASE_APPLIANCE_TEMPLATE_NAME) {
    BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info = readJSON (text: BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info)
    BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info.template_name = params.BASE_APPLIANCE_TEMPLATE_NAME
    BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info = JsonOutput.toJson(BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info)
}

APPLIANCE_CONN_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].connection_info
APPLIANCE_ENV_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info


try {

    def appliance_ip = CI.deploy_base_appliance(
        params.PLATFORM,
        APPLIANCE_CONN_INFO,
        APPLIANCE_ENV_INFO,
        params.VM_IDENTIFIER)

    CI.build_appliance(
        appliance_ip,
        params.EXPORT_PROVIDERS,
        params.IMPORT_PROVIDERS,
        params.CUSTOM_REPO_OWNER_NAMES,
        params.CUSTOM_REPO_BRANCHES,
        params.CORIOLIS_DOCKER_REPO_URL,
        params.CORIOLIS_DOCKER_REPO_BRANCH)

    CI.deploy_appliance(
        appliance_ip,
        params.CORIOLIS_DOCKER_REPO_URL,
        params.CORIOLIS_DOCKER_REPO_BRANCH)

    def appliance_ssh_info = CI.get_appliance_default_ssh_info(appliance_ip)
    CI.enable_console_script(appliance_ssh_info)
    CI.new_appliance_password(appliance_ssh_info)
    CI.cleanup_appliance_logs(appliance_ssh_info)
    CI.cleanup_appliance_landscape_sysinfo_cache(appliance_ssh_info)

    if (params.EXPORT_APPLIANCE == true) {
        CI.export_appliance(
            params.PLATFORM,
            APPLIANCE_CONN_INFO,
            APPLIANCE_ENV_INFO,
            params.VM_IDENTIFIER)
    }

    if (params.REBOOT_APPLIANCE == true) {
        CI.reboot_appliance(appliance_ssh_info)
    }

} catch (Exception e) {
    if (params.CLEANUP_FAILED_APPLIANCE == true) {
        CI.cleanup_appliance(
            params.PLATFORM,
            APPLIANCE_CONN_INFO,
            APPLIANCE_ENV_INFO,
            params.VM_IDENTIFIER)
    } else {
        println("Job parameter CLEANUP_FAILED_APPLIANCE is false.")
        println("The appliance deletion stage will be skipped.")
    }
    throw e
}
